-- greet.adb
-- author: Amazir Chaouchi
-- last modified by: Amazir Chaouchi
-- last modification: 2023-11-26
--
-- Purpose of this program:
--   This program, which is a sub-program, prints "Hello World!" in the console log.
--   Commentaries here describe keywords used to achieve this simple goal.
--
-- Dependency :
--   Ada.Text_IO         --> Put_Line
--   Ada.Integer_Text_IO --> Get
--                       --> Put
--

with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

procedure Check_Positive is
   value : Integer;
begin
   Put("Enter an integer value: ");
   Get(value);

   -- The if structure begins with the reserved word "if", followed by a condition and the keyword "then".
   if(value > 0) then
      Put(value);
      Put_Line(" is a positive value.");
   -- if structures end
   end if;
end Check_Positive;
