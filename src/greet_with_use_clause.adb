-- greet_with_use_clause.adb
-- author: Amazir Chaouchi
-- last modified by: Amazir Chaouchi
-- last modification: 2023-11-26
--
-- Purpose of this program:
--   This program prints "Hello World!" in the console log.
--   greet_with_use_clause.adb is a variation of greet.adb that uses the "use" keyword.
--
-- Dependency :
--   Ada.Text_IO --> Put_Line
--

-- The "use" clause is used right after a reference to the external module that we use.
with Ada.Text_IO; use Ada.Text_IO;


procedure Greet_with_use_clause is
begin
   Ada.Text_IO.Put_Line("======== ENTER: Greet_with_use_clause =========================");

   -- With the "use" clause defined earlier, it is not necessary anymore to prefix Put_Line procedure with the package name
   -- from which it belongs.
   Put_Line("Hello World! (with use clause)");

   Ada.Text_IO.Put_Line("======== EXIT:  Greet_with_use_clause =========================");
end Greet_with_use_clause;
