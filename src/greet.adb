-- greet.adb
-- author: Amazir Chaouchi
-- last modified by: Amazir Chaouchi
-- last modification: 2023-11-26
--
-- Purpose of this program:
--   This program, which is a sub-program, prints "Hello World!" in the console log.
--   Commentaries here describe keywords used to achieve this simple goal.
--
-- Dependency :
--   Ada.Text_IO --> Put_Line
--

-- "with" keyword is used to reference external modules.
-- This keyword is similar to "#include" in C or C++, or "import" in Python.
-- "Ada.Text_IO" package contains procedures (and maybe functions) that helps printing text to the screen.
with Ada.Text_IO;

-- "procedure" is a type of subprogram that does not return a value, like "void" functions in C.
-- Contrary to the way C or C++ work, a program's entry point does not have to be called "main".
-- It can be any valid name we choose, like "Greet" here.
procedure Greet is
begin
   Ada.Text_IO.Put_Line("======== ENTER: Greet ==========================================");

   -- Put_Line is the equivalent to C's printf function.
   -- It is prefixed with "Ada.Text_IO." because it belongs to "Ada.Text_IO" module.
   Ada.Text_IO.Put_Line("Hello World!");

   Ada.Text_IO.Put_Line("======== EXIT:  Greet ==========================================");
-- End is followed by procedure's name
end Greet;

-- NOTE: There is no multi-line comment in Ada.
