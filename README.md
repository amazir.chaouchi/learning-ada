
# Learning Ada

## Building a program

GNAT's builder is called `gprbuild`, and takes the file to build as parameter.

## Detail of elements learn per program

The list of concepts illustrated by each program are listed in the table below.

|Program                                                    |Concept(s)|
|:---                                                       |:---|
|[`greet`](./src/greet.adb)                                 ||
|[`greet_with_use_clause`](./src/greet_with_use_clause.adb) |`use`|
|[`check_positive`](./src/check_positive.adb)               ||
